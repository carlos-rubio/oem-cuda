/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "config.h"
#include <stdio.h>
#include "kernel.h"

void printConfig(Config config){
    printf("\nConfiguration: \n");      
    printf("  Group: %s \n",groupNames[config.group]);      
    printf("  Kernel: %s \n",kernelNames[config.kernel]);      
    printf("  Hypercube dimensions: %d \n",config.dimensions); 
    printf("  Hypercube side: %d \n",config.side); 
    printf("  Kernel divisor: %d \n",config.divisor); 
}

/*
* creates a small grid to estimate execution time
*/
Config createTestConfig(Config config){
    Config testConfig = config;
    testConfig.dimensions = 4;
    testConfig.side = 24;
    testConfig.divisor = 1;
    testConfig.test = true;
    testConfig.doExport = config.doExport;
    return testConfig;
}

/*
* gets the ratio between the number of hypercube points
* of two different configurations
*/
double getRatio(Config config, Config testConfig){
    double real = pow(config.side, config.dimensions);
    double test = pow(testConfig.side, testConfig.dimensions);
    return real/test;
}