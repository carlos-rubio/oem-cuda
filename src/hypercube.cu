/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include "hypercube.h"
#include "index.h"
#include "variables.h"
#include "macro.h"

/*
* returns the index of the global minima of a hypercube
* returns: argmin( abs(hypercube) )
*/
Index findMinimumAbs(Hypercube hypercube, Config config){
    int index = 0;
    float error;
    float minError = fabsf(hypercube[0]); 

    for(unsigned long i=0;i<hypersize(config);i++){
        error = fabsf(hypercube[i]);
        if(error < minError){
            minError = error;                        
            index = i;
        }
    }    
    return fromLinearIndex(index, config);  
}

/*
* returns the index of the minimun value in the errors array
*/
int findMinimum(Errors errors){
    int index = 0;
    float minError;
    bool init = false;

    for(int i=0;i<ERROR_ARRAY_SIZE;i++){        
        if(errors[i].initialized && (!init || errors[i].error < minError)){
            minError = errors[i].error;
            index = i;  
            init = true;
        }
    }    
    return index;  
}

/*
* total sum of an hypercube points
*/
float hypercubeSum(Hypercube hypercube, Config config){
    float sum = 0;
    for(unsigned long i=0;i<hypersize(config);i++){
        sum += hypercube[i];
    }
    return sum;
}

/*
* prints the real values for a n-dimensional index coordinates
*/
void printAxisValues(Index index, Info info){
    printf("\nValues:\n");
    for(int i=0;i<info.numAxis;i++){
        float value = info.axis[i].min + info.axis[i].M * (float)(index.i[i]);   
        printf("  Axis %d, value: %f \n", (i+1), value);
    }
}

/*
* exports the hypercube as a float[] in linear order
* each point is exported as float with the format and endianess 
* of the local machine
* in linux with Intel/AMD64 arch, each point is:
*   4 bytes, IEE754 simple precision and little-endian
*/
void exportHypercube(Hypercube hypercube, const char* fileName, Config config){
    FILE *file;
    file = fopen(fileName, "w");
    if(file == NULL)
        HANDLE_ERROR(IO_ERROR);

    unsigned long nelems = hypersize(config);
    if(fwrite(hypercube, sizeof(float), nelems, file) != nelems)
        HANDLE_ERROR(IO_ERROR);

    fclose(file);    
}

