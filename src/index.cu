/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "index.h"
#include "config.h"
#include <stdio.h>

/*
* returns the number of points of a hypercube
* using config as configuration
*/
unsigned long hypersize(Config config){
    unsigned long ret = config.side;
    for(int i=1;i<config.dimensions;i++)
        ret *= config.side;
    return ret;
}

/*
* host function to convert
* from n-dimensions index to linear index
*/
unsigned long toLinearIndex(Index index, Config config){
    unsigned long d3 = (config.dimensions > 2)? config.side: 1;
    unsigned long d4 = (config.dimensions > 3)? config.side: 1;
    unsigned long d5 = (config.dimensions > 4)? config.side: 1;
    unsigned long d6 = (config.dimensions > 5)? config.side: 1;
    return index.i1 + 
           index.i2 * config.side + 
           index.i3 * config.side * d3 +
           index.i4 * config.side * d3 * d4 + 
           index.i5 * config.side * d3 * d4 * d5 +
           index.i6 * config.side * d3 * d4 * d5 * d6;
}

/*
* host function to convert
* from linear index to n-dimensions index
*/
Index fromLinearIndex(unsigned long idx, Config config){
    unsigned long d3 = (config.dimensions > 2)? config.side: 1;   
    unsigned long d4 = (config.dimensions > 3)? config.side: 1;
    unsigned long d5 = (config.dimensions > 4)? config.side: 1;
    unsigned long d6 = (config.dimensions > 5)? config.side: 1;
    Index ret;
    ret.i1 = idx % config.side;
    ret.i2 = (idx / config.side) % config.side;
    ret.i3 = (config.dimensions > 2)? (idx / (config.side * d3)) % config.side : 0;
    ret.i4 = (config.dimensions > 3)? (idx / (config.side * d3 * d4)) % config.side : 0;
    ret.i5 = (config.dimensions > 4)? (idx / (config.side * d3 * d4 * d5)) % config.side: 0;
    ret.i6 = (config.dimensions > 5)? (idx / (config.side * d3 * d4 * d5 * d6)): 0;
    return ret;
}

/*
* prints the coordinates of a n-dimensional index
*/
void printIndex(Index index, Config config){
    printf("\nIndex: ");
    for(int i=0;i<config.dimensions;i++){
        printf("%d", index.i[i]);
        if(i<(config.dimensions-1)) 
            printf(", ");
    }
    printf("\n");
}




