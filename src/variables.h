/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef VARIABLES_H
#define VARIABLES_H

#include "config.h"
#include "index.h"

typedef float* Variable;

// hypercube side axis
typedef struct {
  float min; // axis minimum (one end of the axis)
  float max; // axis maximum (the other end)
} InputAxis; // used for read an axis from the input file

typedef struct {
    int group; // kernel grop
    int kernel; // kernel id
    int dimensions; // number of dimensions 2,3,4,5 or 6
    int side; // hypercube side
    int numValues; // num values in the input fie
    int numVariables; // num of variables in the input file
    int sizeVariables; // num of values in each variable

    float* parameters; // parameters (values)
    InputAxis* axis; // axis data (min and max values of each axis)
    float* data; // variables data
    } Input; // store the readed information from the input file

typedef struct {
    int num; // num of variables
    int size; // num of values in each variable

    float* data; // variables data
 } Variables; // variables as they are sent to the kernel

typedef struct {
    float min; // axis minimum (one end of the axis)
    float max; // axis maximum (the other end)
    float M; // axis slope = (max - min) / side
} Axis; // axis as they are sent to the kernel

typedef struct {
    int numValues; // num of values
    float* values; // parameters (values) data
    int numAxis; // num of axis
    Axis* axis; // axis data
} Info; // parameters and axis as they are sent to the kernel

typedef struct {
    int mutex; // mutex for this cell
    float error; // cell error
    bool initialized; // if is initalized
    Index index; // index of the minimum
} ErrorCell; // size 36 bytes

// 576 Kbytes, hypercube 4D, with side 48 has 6912 blocks
// so with 16384 no collision with 4D - 48 side
static const int ERROR_ARRAY_SIZE = 16384; 

// stores the errors result of the threads reduction process, filled in GPU
// after that is copied to host to calculate the global minimum
typedef ErrorCell* Errors;

#endif