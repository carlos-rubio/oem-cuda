/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "macro.h"
#include "device.h"
#include "kernel.h"
#include "limits.h"
#include "config.h"
#include <math.h>

static void printLimits(int dimensions, cudaDeviceProp prop);

static int getMaxSidePerThreads(int dimensions, cudaDeviceProp prop);

static int getMaxSidePerMemory(int dimensions, cudaDeviceProp prop);

static bool isValid(int dimensions, int side, cudaDeviceProp prop);

// not declared as unsigned integer because its
// used in divisions where we want decimal results
static const double GB = 1024*1024*1024;

/*
* selects the GPU with the max number of multiprocessors
*/
int selectBestDevice(){    
    int maxMultiprocessors = 0;
    int device = -1;
    cudaDeviceProp prop;
    int count;

    if(cudaGetDeviceCount(&count) != cudaSuccess)
        HANDLE_NO_GPU();

    for(int i=0;i<count;i++){
        HANDLE_CUDA(cudaGetDeviceProperties(&prop, i));
        if (maxMultiprocessors < prop.multiProcessorCount) {
            maxMultiprocessors = prop.multiProcessorCount;
            device = i;
          }        
    }
    
    return device;
}

/*
* gets the maximum number of threads per block for one local CUDA GPU
*/
int getMaxThreadsPerBlock(int device){
    cudaDeviceProp prop;
    HANDLE_CUDA(cudaGetDeviceProperties(&prop, device));
    return prop.maxThreadsPerBlock;
}

/*
* prints some info of all detected CUDA GPUs
*/
void printDevices(void){    
    int count;

    if(cudaGetDeviceCount(&count) != cudaSuccess)
        HANDLE_NO_GPU();

    for(int i=0;i<count;i++){
       printDevice(i);   
    }
}

/*
* prints some info of one local CUDA GPUs
*/
void printDevice(int device){
    
    cudaDeviceProp prop;
    HANDLE_CUDA(cudaGetDeviceProperties(&prop, device));    
    printf("\nDevice %d, %s, rev: %d.%d\n",device, prop.name, prop.major, prop.minor);    
    printf("  max threads per block %d\n", prop.maxThreadsPerBlock);    
    printf("  max threads %d %d %d\n", prop.maxThreadsDim[0],prop.maxThreadsDim[1],prop.maxThreadsDim[2]);            
    printf("  max blocks %d %d %d\n", prop.maxGridSize[0],prop.maxGridSize[1],prop.maxGridSize[2]);
    printf("  multiprocessors %d\n", prop.multiProcessorCount);    

    size_t freeMemory;
    size_t totalMemory;
    HANDLE_CUDA(cudaSetDevice(device));      
    HANDLE_CUDA(cudaMemGetInfo(&freeMemory, &totalMemory));
    printf("  total memory: %.2f GB\n", (totalMemory / GB));
}

/*
* prints the maximum side for each number of dimensions for a GPU device
* the maximum side can be limited by GPU memory of by number of threads/blocks
* with the current grid algorithm
*/
void printLimits(int device){
    cudaDeviceProp prop;
    HANDLE_CUDA(cudaGetDeviceProperties(&prop, device));
    printf("\nLimits:\n"); 
   
    for(int dimensions=2;dimensions<=6;dimensions++){
        printLimits(dimensions, prop);
    }

    printf("Max side per GPU memory only apply with the -o option.\n"); 
}

static void printLimits(int dimensions, cudaDeviceProp prop){
    int maxPerThreads = getMaxSidePerThreads(dimensions,prop);
    int maxPerMemory = getMaxSidePerMemory(dimensions,prop);
    int maxSide = maxPerThreads;
    if(maxPerMemory < maxSide) maxSide = maxPerMemory;
    Config config;
    config.dimensions = dimensions;
    config.side = maxSide;
    unsigned long hypercubeSize = hypersize(config) * sizeof(float);

    printf("  Dimensions: %d\n",dimensions);
    printf("  Max side per GPU threads: %d\n",maxPerThreads);
    printf("  Max side per GPU memory: %d\n",maxPerMemory);
    printf("  Hypercube size (side = %d): %.2f GB\n",maxSide, (hypercubeSize / GB));
    printf("\n");
}

static int getMaxSidePerThreads(int dimensions, cudaDeviceProp prop){
    int maxSide = prop.maxGridSize[1]; // dim2
    maxSide = (dimensions > 2 && prop.maxThreadsDim[0] < maxSide)? prop.maxThreadsDim[0]: maxSide; // dim3
    maxSide = (dimensions > 5 && prop.maxGridSize[2] < maxSide)? prop.maxGridSize[2]: maxSide; // dim6
    
    while(!isValid(dimensions, maxSide, prop))
        --maxSide;

    return maxSide;
}

static bool isValid(int dimensions, int side, cudaDeviceProp prop){
    int maxThreadsPerBlock = prop.maxThreadsPerBlock;
    int divisor = selectDivisor(dimensions, side, maxThreadsPerBlock);
    int d4 = (dimensions >= 4)? side: 1;
    int d5 = (dimensions >= 5)? side: 1;

    if(d4*d5*divisor > prop.maxGridSize[0])
        return false;
    else    
        return true;
}

static int getMaxSidePerMemory(int dimensions, cudaDeviceProp prop){
    size_t freeMemory;
    size_t totalMemory;
    HANDLE_CUDA(cudaMemGetInfo(&freeMemory, &totalMemory));
    double maxSide = pow(freeMemory/sizeof(float), (1.0/dimensions));        
    return (int)maxSide;
}


