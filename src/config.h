/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
    int group; // kernel group
    int kernel; // kernel id
    int dimensions; // number of dimensions 2,3,4,5 or 6
    int side; // hypercube side
    int divisor; // kernel threads divisor
    bool test; // is used for estimate execution time
    bool doExport; // is used if need to copy the hypercube from device to host
    } Config;

void printConfig(Config config);

/*
* creates a small grid to estimate execution time
*/
Config createTestConfig(Config config);

/*
* gets the ratio between the number of hypercube points
/ iof two different configurations
*/
double getRatio(Config config, Config testConfig);

#endif