/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef UTIL_H
#define UTIL_H

/*
* removes empty characters from the beginning of a string
*   tabs, new line, carriage return, spaces and page breaks
*/
char* ltrim(char* str);

/*
* removes empty characters from the end of a string
*   tabs, new line, carriage return, spaces and page breaks
*/
char* rtrim(char* str);

/*
* removes empty characters from both sides of a string
*   tabs, new line, carriage return, spaces and page breaks
*/
char* trim(char* str);

/*
* prints pretty time given it in ms
* writes result in buffer
*/
void formatTime(char* buffer, float ms);

#endif