/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef KERNEL_H
#define KERNEL_H
#include "variables.h"
#include "config.h"
#include "hypercube.h"
#include <stdio.h>

typedef struct {
    dim3 blocks;
    dim3 threads;
} Grid;

typedef struct {
    int threadx;
    int thready;
    float error;
} ThreadError;

typedef struct {
    float elapsedTime;
    cudaError_t error;
} Execution;

extern __shared__ ThreadError shared[];

/*
* selects a divisor used to part the dimension 1 between blocks.x
* and threads.y without exceding the maximum number of threads
* per block in the grid
*/
int selectDivisor(int dimensions, int side, int maxThreadsPerBlock);

/*
* Creates the execution grid
* the dimensions are encoded in the execution grid as:
*
*                 x          y         z
*  blocks:  { fn(d4,d5,d1), d2,       d6 }
*  threads: {    d3,        d1,    not_used }
*
*/
Grid createGrid(Config config);

/*
* prints the execution grid
*/
void printGrid(Grid grid);

/*
* device fn to interpolate the real value of an axis given parameters configuration
*/
__forceinline__ __device__ float interpolate(int axis, Index index, Info info){    
    return info.axis[axis].min + info.axis[axis].M * (float)(index.i[axis]);    
}

/*
* device fn to access a variable (as a float[])
*/
__forceinline__ __device__ float* variable(int index, Variables variables){    
    return &(variables.data[index*variables.size]);
}

__forceinline__ __device__ void lock(int* mutex){
    while( atomicCAS(mutex, 0, 1) != 0);
}

__forceinline__ __device__ void unlock(int* mutex){
    atomicExch(mutex, 0);
}

__forceinline__ __device__ void update(float error, Index index, Config config, Hypercube hypercube, Errors errors){

    // store error in the hypercube
    if(config.doExport)
        hypercube[ linearIndex(index, config)] = error; 

    // store thread error
    int numThreads = blockDim.x * blockDim.y;
    int threadIndex = threadIdx.y * blockDim.x + threadIdx.x;
    shared[threadIndex].error = fabs(error);
    shared[threadIndex].threadx = threadIdx.x;
    shared[threadIndex].thready = threadIdx.y;

    __syncthreads();

    // reduction process between threads
    int pending = numThreads;
    while(pending > 1){
        pending = (pending + 1) / 2;
        if(threadIndex < pending){
            int index = threadIndex + pending;
            // consolidate error for threadIndex and threadIndex + pending
            if( index < numThreads && shared[index].error < shared[threadIndex].error){
                shared[threadIndex].error = shared[index].error;
                shared[threadIndex].threadx = shared[index].threadx;
                shared[threadIndex].thready = shared[index].thready;
            }
        }
        __syncthreads();
    }

    // thread 0 update the errors array value
    if(threadIndex == 0){
        int blockId = blockIdx.y * gridDim.x + blockIdx.x;
        int i = blockId % ERROR_ARRAY_SIZE;

        lock(&errors[i].mutex);
    
        if(!errors[i].initialized || errors[i].error > shared[0].error){
            errors[i].initialized = true;
            errors[i].error = shared[0].error;
            errors[i].index = index;
            // update thread dimensions based on thread info in shared memory
            errors[i].index.i1 = (blockIdx.x % config.divisor) + (shared[0].thready * config.divisor);;
            errors[i].index.i3 = shared[0].threadx;
        }
        unlock(&errors[i].mutex);
    }
}

/*
* invokes the kernel given a configuration, parameters, variables, output hypercube and output errors
* waits for all kernel threads to complete
* and measure execution time. Return exectution time and kernel error code.
*/
Execution dispatch(Config config, Info info, Variables variables , Hypercube hypercube, Errors errors);

// ---------------------------- Kernel configs --------------------------------

typedef enum Groups {DEFAULT, END_G} Group;
static const char* groupNames[] = {"default"};

typedef enum Kernels { UNKNOWN, WIND_BOX, END_K } Kernel;
static const char* kernelNames[] = {"unknown","windBox"};

__global__ void windBox(Config config, Info info, Variables variables , Hypercube hypercube, Errors errors);

// ---------------------------- Kernel configs --------------------------------

/*
* gets the group enum value from its name
*/
Group parseGroup(char* name);

/*
* gets the kernel enum value from its name
*/
Kernel parseKernel(char* name);

#endif