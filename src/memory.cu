/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "memory.h"
#include "index.h"
#include "macro.h"
#include "variables.h"
#include <stdlib.h>

// --------------- Host ----------------------------

void allocMemoryInput(Input* input){
    size_t sizeParameters = input->numValues * sizeof(float);
    size_t sizeAxis = input->dimensions * sizeof(InputAxis);
    size_t sizeData = input->numVariables * input->sizeVariables * sizeof(float);

    input->parameters = (float*)malloc(sizeParameters);
    if(NULL ==  input->parameters){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    input->axis = (InputAxis*)malloc(sizeAxis);
    if(NULL ==  input->axis){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    input->data = (float*)malloc(sizeData);    
    if(NULL ==  input->data){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeMemoryInput(Input* input){
    free(input->parameters);
    free(input->axis);
    free(input->data);    
} 

void allocParametersHost(Info* parameters){
    // Values
    size_t sizeValues = parameters->numValues * sizeof(float);    
    parameters->values = (float*)malloc(sizeValues);    
    if (NULL == parameters->values){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    // Axis
    size_t sizeAxis = parameters->numAxis * sizeof(Axis);
    parameters->axis = (Axis*)malloc(sizeAxis);
    if (NULL == parameters->axis){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeParametersHost(Info* parameters){
    free(parameters->values);
    free(parameters->axis);
}

void allocHypercubeHost(Hypercube* hypercubeHostPtr, Config config){
    *hypercubeHostPtr = (Hypercube)malloc( hypersize(config) * sizeof(float) ); 
    if (NULL == *hypercubeHostPtr){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void allocErrorsHost(Errors* errorHostPtr){
    *errorHostPtr = (Errors)malloc( ERROR_ARRAY_SIZE * sizeof(ErrorCell) ); 
    if (NULL == *errorHostPtr){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeHypercubeHost(Hypercube hypercubeHost){
    free(hypercubeHost);
}

void freeErrorsHost(Errors errors){
    free(errors);
}

void allocVariablesHost(Variables* variables){
    size_t size = variables->num * variables->size * sizeof(float);
    variables->data = (float*)malloc(size);
    if (NULL == variables->data){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeVariablesHost(Variables* variables){
    free(variables->data);
}


// --------------- Device ----------------------------

void allocHypercubeDevice(float** hypercubeDevicePtr, Config config){    
    HANDLE_CUDA( cudaMalloc( hypercubeDevicePtr, hypersize(config) * sizeof(float) ) );
}

void allocErrorsDevice(Errors* errorsDevicePtr){
    HANDLE_CUDA( cudaMalloc( errorsDevicePtr, ERROR_ARRAY_SIZE * sizeof(ErrorCell) ) );
    HANDLE_CUDA( cudaMemset( *errorsDevicePtr, 0, ERROR_ARRAY_SIZE * sizeof(ErrorCell) ) );
}

void freeDeviceVariables(Variables* variables){
    HANDLE_CUDA( cudaFree(variables->data) );
}

void freeDeviceParams(Info* parametersDevice){
    HANDLE_CUDA( cudaFree( parametersDevice->values) );
    HANDLE_CUDA( cudaFree( parametersDevice->axis) );    
}

void freeDeviceHypercube(float* hypercubeDevice){
    HANDLE_CUDA( cudaFree( hypercubeDevice) );
}

void freeDeviceErrors(Errors errorsDevice){
    HANDLE_CUDA( cudaFree( errorsDevice) );
}

// -------- Copy host to device -------------------

void copyVariablesToDevice(Variables* variables, Variables* variablesDevice){
    size_t size = variables->num * variables->size * sizeof(float);    
    HANDLE_CUDA( cudaMalloc( &variablesDevice->data, size) );
    HANDLE_CUDA( cudaMemcpy( variablesDevice->data , variables->data, size, cudaMemcpyHostToDevice) );    
}

void copyParametersToDevice(Info* parameters, Info* parametersDevice){
    size_t sizeValues = parameters->numValues * sizeof(float);        
    HANDLE_CUDA( cudaMalloc( &parametersDevice->values, sizeValues) );
    HANDLE_CUDA( cudaMemcpy( parametersDevice->values, parameters->values, sizeValues, cudaMemcpyHostToDevice) );

    size_t sizeAxis = parameters->numAxis * sizeof(Axis);   
    HANDLE_CUDA( cudaMalloc( &parametersDevice->axis, sizeAxis) );
    HANDLE_CUDA( cudaMemcpy( parametersDevice->axis, parameters->axis, sizeAxis, cudaMemcpyHostToDevice) );
}

// ------------ Copy device to Host -----------

void copyHypercubeToHost(float* hypercubeDevice, Hypercube hypercube, Config config){
    HANDLE_CUDA( cudaMemcpy( hypercube, hypercubeDevice, hypersize(config) * sizeof(float) , cudaMemcpyDeviceToHost) );
}

void copyErrorsToHost(ErrorCell* errorsDevice, Errors errors){
    HANDLE_CUDA( cudaMemcpy( errors, errorsDevice, ERROR_ARRAY_SIZE * sizeof(ErrorCell) , cudaMemcpyDeviceToHost) );
}