/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef HYPERCUBE_H
#define HYPERCUBE_H

#include "index.h"
#include "variables.h"

typedef float* Hypercube;

/*
* returns the index of the global minima of a hypercube
* returns: argmin( abs(hypercube) )
*/
Index findMinimumAbs(Hypercube hypercube, Config config);

/*
* returns the index of the minimun value in the errors array
*/
int findMinimum(Errors errors);

/*
* total sum of an hypercube points
*/
float hypercubeSum(Hypercube hypercube, Config config);

/*
* prints the real values for a n-dimensional index coordinates
*/
void printAxisValues(Index index, Info info);

/*
* exports the hypercube as a float[] in linear order
* each point is exported as float with the format and endianess 
* of the local machine
* in linux with Intel/AMD64 arch, each point is:
*   4 bytes, IEE754 simple precision and little-endian
*/
void exportHypercube(Hypercube hypercube, const char* fileName, Config config);

#endif