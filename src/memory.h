/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MEMORY_H
#define MEMORY_H

#include "variables.h"
#include "hypercube.h"

// ------------------ Host ------------------------

void allocMemoryInput(Input* input);

void freeMemoryInput(Input* input);

void allocParametersHost(Info* parameters);

void freeParametersHost(Info* parameters);

void allocHypercubeHost(Hypercube* hypercubeHostPtr, Config config);

void allocErrorsHost(Errors* errorHostPtr);

void freeHypercubeHost(Hypercube hypercubeHost);

void freeErrorsHost(Errors errors);

void allocVariablesHost(Variables* variables);

void freeVariablesHost(Variables* variables);


// ----------- Device  --------------------------

void allocHypercubeDevice(float** hypercubeDevicePtr, Config config);

void allocErrorsDevice(Errors* errorsDevicePtr);

void freeDeviceVariables(Variables* variables);

void freeDeviceParams(Info* parametersDevice);

void freeDeviceHypercube(float* hypercubeDevice);

void freeDeviceErrors(Errors errorsDevice);


// -------- Copy host to device -------------------

void copyParametersToDevice(Info* parameters, Info* parametersDevice);

void copyVariablesToDevice(Variables* variables, Variables* variablesDevice);


// -------- Copy device to host -------------------

void copyHypercubeToHost(float* hypercubeDevice, Hypercube hypercube, Config config);

void copyErrorsToHost(ErrorCell* errorsDevice, Errors errors);


#endif