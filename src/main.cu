/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <math.h>
#include <unistd.h>  
#include <string.h>  
#include "kernel.h"
#include "config.h"
#include "macro.h"
#include "reader.h"
#include "memory.h"
#include "variables.h"
#include "index.h"
#include "device.h"
#include "hypercube.h"
#include "math.h"
#include "util.h"

typedef struct {
    char* inputFile;
    bool doExport;
    bool doInteractive;
    char* outputFile;    
} ExecConfig;

static void printHelp(void);

static void execute(ExecConfig config);

static void printFormat(void);

static bool askForExecution(float estimatedTime);

static bool handleExecResult(Execution result, Config config);

// need to be in the heap due its size
static Hypercube hypercube;

int main(int argc, char **argv) {
    int opt;
    bool valid = false; // valid execution without input file
    
    ExecConfig config;
    config.doExport = false;   
    config.doInteractive = false;
    config.outputFile = NULL;
   
    while (( opt = getopt(argc, argv, "hfildo:")) != -1){
        switch(opt){
            case 'h': printHelp(); valid = true; break;
            case 'd': printDevices(); valid = true; break;
            case 'l': printLimits(selectBestDevice()); valid = true; break;
            case 'f': printFormat(); valid = true; break;
            case 'i': config.doInteractive = true ; break;
            case 'o': 
                config.doExport = true;
                config.outputFile = strdup(optarg);
                break;            
            default:                 
                printHelp(); return -1; // incorrect option
        }
    }

    if (optind >= argc && !valid) {
        printHelp(); return -1; // don't exists input file name
    }

    if(optind < argc) {
        config.inputFile = argv[optind];
    
        execute(config); 

        if(config.outputFile != NULL)
            free(config.outputFile);
    }

    return 0;
}

static void printHelp(void){
    printf("\nUsage: oem [OPTION]... INFILE\n");
    printf("\nOptions:\n");
    printf("\n  -h        show help\n");
    printf("\n  -f        show INFILE required format\n");
    printf("\n  -l        show hypercube limits\n");
    printf("\n  -d        show GPU devices\n"); 
    printf("\n  -i        show estimated execution time and ask for confirmation\n"); 
    printf("\n  -o file   export hypercube to raw file\n");
    printf("\n");
}

static void printFormat(void){
    printf("\nFormat of input file:\n\n");
    printf("  group: groupName\n");
    printf("  kernel: kernelName\n");
    printf("  dimensions: d\n");
    printf("  side: s\n");
    printf("  numValues: n\n");
    printf("  numVariables: m\n");
    printf("  sizeVariables: l\n\n");
    printf("  value: value-1\n");
    printf("  value: value-2\n");
    printf("  ...\n");
    printf("  value: value-n\n\n");
    printf("  axisMin: axisMin-1\n");
    printf("  axisMax: axisMax-1\n");
    printf("  ...\n");
    printf("  axisMin: axisMin-s\n");
    printf("  axisMax: axisMax-s\n\n");
    printf("  1.0 1.0 1.0 ... 1.0 (data of m variables, index 1)\n");
    printf("  ...\n");
    printf("  1.0 1.0 1.0 ... 1.0 (data of m variables, index l)\n");
    printf("\n");
}

static void execute(ExecConfig execConfig){
    // select and print device info
    int device = selectBestDevice();
    printDevice(device);
    HANDLE_CUDA(cudaSetDevice(device));    
    int maxThreadsPerBlock = getMaxThreadsPerBlock(device);
              
    // read input (also allocate input memory in host)
    Input input;
    readFile(execConfig.inputFile, &input);

    // read configuration
    Config config = extractConfig(input);
    config.divisor = selectDivisor(config.dimensions, config.side, maxThreadsPerBlock);
    config.test = false;
    config.doExport = execConfig.doExport;
    printConfig(config);

    // read parameters, axis limits and variables
    Info parameters = extractParameters(input);
    Variables variables = extractVariables(input);
   
    // copy variables to device memory    
    Variables variablesDevice = variables;
    copyVariablesToDevice(&variables, &variablesDevice);

    // copy parameters to device memory
    Info parametersDevice = parameters;
    copyParametersToDevice(&parameters, &parametersDevice);

    // create errors array in device
    Errors errorsDevice;   
    allocErrorsDevice(&errorsDevice);   

    // create hypercube in device
    Hypercube hypercubeDevice;
    if(execConfig.doExport) {
        allocHypercubeDevice(&hypercubeDevice, config);   
    }        

    bool skip = false;
    bool ok = true;
    Execution exec;

    // in interactive mode, estimate execution time and ask user for continue
    if(execConfig.doInteractive){         
        Config testConfig = createTestConfig(config);
        exec = dispatch(testConfig, parametersDevice, variablesDevice, hypercubeDevice, errorsDevice);  
        ok = handleExecResult(exec, testConfig);
        float estimatedTime = exec.elapsedTime * getRatio(config, testConfig);
        skip = askForExecution(estimatedTime);
    }

    if(!skip && ok){
        // invoke the kernel
        exec = dispatch(config, parametersDevice, variablesDevice, hypercubeDevice, errorsDevice);   
        ok = handleExecResult(exec, config);             
    }

    // create errors array in host memory
    Errors errors;
    if(ok){
        allocErrorsHost(&errors);
        // copy device errors to host errors
        copyErrorsToHost(errorsDevice, errors);
        // free device errors
        freeDeviceErrors(errorsDevice);
    }

    if(execConfig.doExport && ok && !skip) {
        // create hypercube in host memory
        allocHypercubeHost(&hypercube, config);
        // copy device hypercube to host hypercube
        copyHypercubeToHost(hypercubeDevice, hypercube, config);
        // free device Hypercube
        freeDeviceHypercube(hypercubeDevice);
    } 

    // Free device memory
    freeDeviceVariables(&variablesDevice);
    freeDeviceParams(&parametersDevice);
    

    if(!skip && ok){
        // print time
        char buffer[50];
        formatTime(buffer, exec.elapsedTime);
        printf("\nElapsed time: %s\n", buffer);

        //float sum = hypercubeSum(hypercube, config);    
        //printf("Hypercube sum: %f \n",sum);
        // find global minimum
        
        int i = findMinimum(errors);
        printIndex(errors[i].index, config);
        printf("Minimum error: %f \n", errors[i].error);
        printAxisValues(errors[i].index, parameters);
        

        // export hypercube as raw data
        if(execConfig.doExport){                    
            exportHypercube(hypercube, execConfig.outputFile, config);    
            printf("\nExported hypercube to: %s\n", execConfig.outputFile);
        }
    }

    // Free host memory
    freeMemoryInput(&input);     
    freeParametersHost(&parameters);  
    freeErrorsHost(errors);

    if(execConfig.doExport && ok) {  
        freeHypercubeHost(hypercube);
    }
}

/*
* shows estimated time and ask to the user
* for continue
*/
static bool askForExecution(float estimatedTime){
    char buffer[50];
    formatTime(buffer, estimatedTime);
    printf("\nEstimated time: %s\n", buffer);
    printf("\nContinue (y/n) ?\n");
    char c = getchar();
    if(c != 'Y' && c!='y')
        return true;
    else
        return false;
}

/*
* checks execution result error
 */
static bool handleExecResult(Execution result, Config config){
    if(result.error == cudaSuccess) return true;

    printf("\nError invoking kernel: %s\n", cudaGetErrorString (result.error) );

    if(!config.test && result.error == cudaErrorInvalidValue){
        printf("   Check CUDAFLAGS parameter in makefile.\n");
        printf("   NVCC uses -arch=sm_20 by default so the limits can be lower that\n");        
        printf("   the indicated  with the -l option for your device.\n");        
        printf("   Update to match your device and compile again.\n");        
    }
    return false;
}

