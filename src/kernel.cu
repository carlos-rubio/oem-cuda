/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "kernel.h"
#include <stdio.h>
#include "macro.h"

/*
* selects a divisor used to part the dimension 1 between blocks.x
* and threads.y without exceding the maximum number of threads
* per block in the grid
*/
int selectDivisor(int dimensions, int side, int maxThreadsPerBlock){
    int minDivisor = ((side * side) / maxThreadsPerBlock) + 1;
    int divisor = side;
    int prevDivisor = divisor;
    while(divisor > 0){ 
        if(side % divisor == 0){
            if(divisor < minDivisor) return prevDivisor;
            prevDivisor = divisor;
        }
        --divisor;        
    }
    return prevDivisor;    
}

/*
* Creates the execution grid
* the dimensions are encoded in the execution grid as:
*
*                 x          y         z
*  blocks:  { fn(d4,d5,d1), d2,       d6 }
*  threads: {    d3,        d1,    not_used }
*
*/
Grid createGrid(Config config){    
    int d3 = (config.dimensions > 2)? config.side: 1;
    int d4 = (config.dimensions > 3)? config.side: 1;
    int d5 = (config.dimensions > 4)? config.side: 1;
    int d6 = (config.dimensions > 5)? config.side: 1;
    dim3 blocks(d4 * d5 * config.divisor, config.side, d6);
    dim3 threads(d3, config.side / config.divisor);
    Grid grid;
    grid.blocks = blocks;
    grid.threads = threads;
    return grid;
}

/*
* prints the execution grid
*/
void printGrid(Grid grid){
    dim3 blocks = grid.blocks;
    dim3 threads = grid.threads;
    int totalBlocks = blocks.x * blocks.y * blocks.z;
    int totalThreads = threads.x * threads.y * threads.z;
    printf("\nKernel grid: \n");      
    printf("  Blocks %d, %d, %d, total: %d \n",blocks.x, blocks.y, blocks.z, totalBlocks);      
    printf("  Threads %d, %d, %d, total: %d \n",threads.x, threads.y, threads.z, totalThreads);          
}

/*
* invokes the kernel given a configuration, parameters, variables, output hypercube and output errors
* waits for all kernel threads to complete
* and measure execution time. Return exectution time and kernel error code.
*/
Execution dispatch(Config config, Info info, Variables variables , Hypercube hypercube, Errors errors){   

    // execution info
    Execution result;

    // Prepare execution grid
    Grid grid = createGrid(config);  
    if(!config.test)
        printGrid(grid);
    
    // time measurement
    cudaEvent_t start, stop;
    HANDLE_CUDA(cudaEventCreate(&start));
    HANDLE_CUDA(cudaEventCreate(&stop));
    HANDLE_CUDA(cudaEventRecord(start, 0)); // start timer 

    size_t sharedSize = sizeof(ThreadError) * grid.threads.x * grid.threads.y * grid.threads.z;
    // ---------------------------- Kernel configs --------------------------------
    
    switch(config.kernel){
        case WIND_BOX: 
            windBox<<<grid.blocks,grid.threads, sharedSize>>>(config, info, variables, hypercube, errors);
            break;
        default:
            HANDLE_ERROR(KERNEL_ERROR);
    }
    // ---------------------------- Kernel configs --------------------------------
    result.error = cudaGetLastError(); 
    cudaDeviceSynchronize();

    HANDLE_CUDA(cudaEventRecord(stop, 0));
    HANDLE_CUDA(cudaEventSynchronize(stop));
    HANDLE_CUDA(cudaEventElapsedTime(&result.elapsedTime, start, stop));    
    HANDLE_CUDA(cudaEventDestroy(start));
    HANDLE_CUDA(cudaEventDestroy(stop));

    return result;
}

/*
* gets the group enum value from its name
*/
Group parseGroup(char* name){
    for(int i=0;i<END_G;i++){
        if(strcmp(name, groupNames[i]) == 0)
            return (Group)i;
    }
    return DEFAULT;
}

/*
* gets the kernel enum value from its name
*/
Kernel parseKernel(char* name){
    for(int i=0;i<END_K;i++){
        if(strcmp(name, kernelNames[i]) == 0)
            return (Kernel)i;
    }
    return UNKNOWN;
}
