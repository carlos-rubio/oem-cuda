NVCC =nvcc
CC=gcc

# CUDAFLAGS = -arch=compute_50 # Maxwell (50, 52, 53)
# CUDAFLAGS = -arch=compute_60 # Pascal (60, 61, 62)
# CUDAFLAGS = -arch=compute_70 # Volta (70, 72)
# CUDAFLAGS = -arch=compute_75 # Turing

CUDAFLAGS = -arch=compute_50

SOURCES = $(wildcard src/*.cu) $(wildcard src/kernels/*.cu)
OBJS := $(patsubst %.cu,%.o,$(SOURCES))

TARGET = oem

.DEFAULT: all

all: link

link: ${OBJS}
	${NVCC} ${CUDAFLAGS} -o ${TARGET} ${OBJS}

%.o: %.cu
	${NVCC} ${CUDAFLAGS} -c $< -o $@

clean:
	rm -f src/*.o 
	rm -f src/kernels/*.o 
	rm -f ${TARGET}
