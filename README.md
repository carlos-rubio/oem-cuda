# OEM-CUDA

Oem-cuda is a system identification framework that runs on a NVIDIA GPU.

Its purpose is to serve as engine for the Output Error Method (OEM).

[How it works](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/How%20it%20works)

[Pros and cons](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Pros%20and%20cons)


## Kernels

You should implement your own kernel for your specific problem:

[Kernel example](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Kernel%20example)

[Add a kernel](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Add%20new%20kernel)


## Running

Your kernel need to be compiled with the rest of the framework:

[Download and compile](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Download%20and%20compile)

Good performance is achieved with any modern NVIDIA GPU, but if you don't have access to a CUDA enabled device or need more power you can use a remote instance:

[Performance overview](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Performance%20overview)

[Using an AWS instance](https://bitbucket.org/carlos-rubio/oem-cuda/wiki/Using%20AWS%20instance)